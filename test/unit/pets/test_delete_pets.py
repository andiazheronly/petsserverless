import os
import sys
import unittest

import boto3
from moto import mock_dynamodb2

import setup_variables

sys.path.append('../../../')
from pets.lambda_function import lambda_handler

AWS_REGION = os.environ.get('AWS_REGION_DEFAULT', 'us-east-1')


class DeletePetsTest(unittest.TestCase):

    def setUp(self) -> None:
        self.__structure_table_pets = setup_variables.structure_table_pets

    @mock_dynamodb2
    def test_delete_success(self):
        self.__create_aws_infra()
        event = {
            "httpMethod": 'DELETE',
            "resource": '/pets/{petId}',
            "pathParameters": {
                "petId": '1122-assd'
            }
        }
        response = lambda_handler(event, None)
        dictionary = {
            'body': '{"message": "Pet deleted"}',
            'headers': {'Access-Control-Allow-Origin': '*'},
            'isBase64Encoded': False,
            'statusCode': 200
        }
        self.assertDictEqual(dictionary, response)

    @mock_dynamodb2
    def test_delete_failed(self):
        self.__create_aws_infra()
        event = {
            "httpMethod": 'DELETE',
            "resource": '/pets/{petId}',
            "pathParameters": {
                "petId": '2sa1-asass'
            }
        }
        response = lambda_handler(event, None)
        dictionary = {
            'body': '{"message": "Pet not found"}',
            'headers': {'Access-Control-Allow-Origin': '*'},
            'isBase64Encoded': False,
            'statusCode': 404
        }
        self.assertDictEqual(dictionary, response)

    def __create_aws_infra(self):
        dynamo_db = boto3.resource('dynamodb', region_name=AWS_REGION)
        self.__table_pets = dynamo_db.create_table(**self.__structure_table_pets)
        self.__table_pets.put_item(
            Item={
                'petId': '1122-assd',
                'name': 'MUNECA2'
            }
        )
        self.__table_pets.put_item(
            Item={
                'petId': '2',
                'name': 'FIRULAIS'
            }
        )
        self.__table_pets.put_item(
            Item={
                'petId': '3',
                'name': 'Terry'
            }
        )


if __name__ == '__main__':
    unittest.main()
