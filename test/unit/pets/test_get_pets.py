import os
import sys
import unittest

import boto3
from moto import mock_dynamodb2

import setup_variables

sys.path.append('../../../')
from pets.lambda_function import lambda_handler

AWS_REGION = os.environ.get('AWS_REGION_DEFAULT', 'us-east-1')


class GetPetsTest(unittest.TestCase):

    def setUp(self) -> None:
        self.__structure_table_pets = setup_variables.structure_table_pets

    @mock_dynamodb2
    def test_get_all_pets_ok(self):
        self.__create_aws_infra()

        event = {
            "httpMethod": 'GET',
            "resource": '/pets'
        }
        response = lambda_handler(event, None)
        dictionary = {
            'body': '{"pets": [{"petId": "1", "name": "FIRULAIS"}, {"petId": "2", "name": "MUNECA", "owner": "Juan '
                    'Andres"}]}',
            'headers': {'Access-Control-Allow-Origin': '*'},
            'isBase64Encoded': False,
            'statusCode': 200
        }
        self.assertDictEqual(dictionary, response)

    @mock_dynamodb2
    def test_get_one_pet_ok(self):
        self.__create_aws_infra()
        event = {
            "httpMethod": 'GET',
            "resource": '/pets/{petId}',
            "pathParameters": {
                "petId": '1'
            }
        }
        response = lambda_handler(event, None)
        dictionary = {
            'body': '{"pet": {"petId": "1", "name": "FIRULAIS"}}',
            'headers': {'Access-Control-Allow-Origin': '*'},
            'isBase64Encoded': False,
            'statusCode': 200
        }
        self.assertDictEqual(dictionary, response)

    @mock_dynamodb2
    def test_get_all_pets_not_fount(self):
        dynamo_db = boto3.resource('dynamodb', region_name=AWS_REGION)
        self.__table_pets = dynamo_db.create_table(**self.__structure_table_pets)

        event = {
            "httpMethod": 'GET',
            "resource": '/pets'
        }
        response = lambda_handler(event, None)
        dictionary = {
            'statusCode': 404,
            'headers': {'Access-Control-Allow-Origin': '*'},
            'body': '{"message": "Pets not found"}',
            'isBase64Encoded': False
        }
        self.assertDictEqual(dictionary, response)

    @mock_dynamodb2
    def test_get_one_pet_not_fount(self):
        self.__create_aws_infra()
        event = {
            "httpMethod": 'GET',
            "resource": '/pets/{petId}',
            "pathParameters": {
                "petId": '32'
            }
        }
        response = lambda_handler(event, None)
        dictionary = {
            'statusCode': 404,
            'headers': {'Access-Control-Allow-Origin': '*'},
            'body': '{"message": "Pet not found"}',
            'isBase64Encoded': False
        }
        self.assertDictEqual(dictionary, response)

    def __create_aws_infra(self):
        dynamo_db = boto3.resource('dynamodb', region_name=AWS_REGION)
        self.__table_pets = dynamo_db.create_table(**self.__structure_table_pets)
        self.__table_pets.put_item(
            Item={
                'petId': '1',
                'name': 'FIRULAIS'
            }
        )
        self.__table_pets.put_item(
            Item={
                'petId': '2',
                'name': 'MUNECA',
                'owner': "Juan Andres"
            }
        )


if __name__ == '__main__':
    unittest.main()
