import os

DB_TABLE_PETS = os.environ.get('DB_TABLE_PETS', 'pets')


structure_table_pets = {
    'TableName': DB_TABLE_PETS,
    'KeySchema': [
        {
            'AttributeName': 'petId',
            'KeyType': 'HASH'
        }
    ],
    'AttributeDefinitions': [
        {
            'AttributeName': 'petId',
            'AttributeType': 'S'
        }
    ],
    'ProvisionedThroughput': {
        'ReadCapacityUnits': 1,
        'WriteCapacityUnits': 1
    }
}
