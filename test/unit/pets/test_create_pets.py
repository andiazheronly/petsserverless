import os
import sys
import unittest

import boto3
from moto import mock_dynamodb2

import setup_variables

sys.path.append('../../../')
from pets.lambda_function import lambda_handler

AWS_REGION = os.environ.get('AWS_REGION_DEFAULT', 'us-east-1')


class CreatePetsTest(unittest.TestCase):
    def setUp(self) -> None:
        self.__structure_table_pets = setup_variables.structure_table_pets

    @mock_dynamodb2
    def test_post_one_new_pet(self):
        self.__create_aws_infra()
        event = {
            "httpMethod": 'POST',
            "resource": '/pets',
            "body": '{"name": "MUNECA", "owner": "Juan Andres"}'
        }
        response = lambda_handler(event, None)
        self.assertEqual(200, response['statusCode'])
        self.assertIn("MUNECA", response['body'])
        self.assertIn("Juan Andres", response['body'])

    @mock_dynamodb2
    def test_post_one_new_pet_failed_1(self):
        self.__create_aws_infra()
        event = {
            "httpMethod": 'POST',
            "resource": '/pets',
            "body": '{"name": "MUÑECA", "owner": ""}'
        }
        response = lambda_handler(event, None)
        self.assertEqual(200, response['statusCode'])
        self.assertIn("MU\\u00d1ECA", response['body'])
        self.assertNotIn("owner", response['body'])

    @mock_dynamodb2
    def test_post_one_new_pet_failed_2(self):
        self.__create_aws_infra()
        event = {
            "httpMethod": 'POST',
            "resource": '/pets',
        }
        response = lambda_handler(event, None)
        dictionary = {
            'statusCode': 503,
            'headers': {'Access-Control-Allow-Origin': '*'},
            'body': '{"message": "Internal Server Error"}',
            'isBase64Encoded': False
        }
        self.assertDictEqual(dictionary, response)

    def __create_aws_infra(self):
        dynamo_db = boto3.resource('dynamodb', region_name=AWS_REGION)
        self.__table_pets = dynamo_db.create_table(**self.__structure_table_pets)


if __name__ == '__main__':
    unittest.main()
