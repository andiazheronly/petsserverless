import sys
import unittest

from moto import mock_dynamodb2

sys.path.append('../../../')
from pets.lambda_function import lambda_handler


class PetsTest(unittest.TestCase):

    @mock_dynamodb2
    def test_bad_method(self):
        event = {
            "httpMethod": 'OPTIONS',
            "resource": '/pets'
        }
        response = lambda_handler(event, None)
        dictionary = {
            'statusCode': 405,
            'headers': {'Access-Control-Allow-Origin': '*'},
            'body': '{"message": "Method not allowed"}',
            'isBase64Encoded': False
        }
        self.assertDictEqual(dictionary, response)

    @mock_dynamodb2
    def test_bad_resource(self):
        event = {
            "httpMethod": 'GET',
            "resource": '/any_resource'
        }
        response = lambda_handler(event, None)
        dictionary = {
            'statusCode': 400,
            'headers': {'Access-Control-Allow-Origin': '*'},
            'body': '{"message": "Bad request"}',
            'isBase64Encoded': False
        }
        self.assertDictEqual(dictionary, response)

    @mock_dynamodb2
    def test_bad_resource_one_pet_2(self):
        event = {
            "httpMethod": 'GET',
            "resource": '/any_resource/{petId}',
            "pathParameters": {
                "petId": '1'
            }
        }
        response = lambda_handler(event, None)
        dictionary = {
            'statusCode': 400,
            'headers': {'Access-Control-Allow-Origin': '*'},
            'body': '{"message": "Bad request"}',
            'isBase64Encoded': False
        }
        self.assertDictEqual(dictionary, response)

    @mock_dynamodb2
    def test_bad_resource_one_pet_1(self):
        event = {
            "httpMethod": 'GET',
            "resource": '/pets/{petId}',
            "pathParameters": {
                "any": '1'
            }
        }
        response = lambda_handler(event, None)
        dictionary = {
            'statusCode': 400,
            'headers': {'Access-Control-Allow-Origin': '*'},
            'body': '{"message": "Bad request"}',
            'isBase64Encoded': False
        }
        self.assertDictEqual(dictionary, response)

    @mock_dynamodb2
    def test_bad_resource_one_pet(self):
        event = {
            "httpMethod": 'GET',
            "resource": '/pets/{any}',
            "pathParameters": {
                "petId": '1'
            }
        }
        response = lambda_handler(event, None)
        dictionary = {
            'statusCode': 400,
            'headers': {'Access-Control-Allow-Origin': '*'},
            'body': '{"message": "Bad request"}',
            'isBase64Encoded': False
        }
        self.assertDictEqual(dictionary, response)

    @mock_dynamodb2
    def test_get_one_pet_no_body(self):
        response = lambda_handler({}, None)
        dictionary = {
            'statusCode': 503,
            'headers': {'Access-Control-Allow-Origin': '*'},
            'body': '{"message": "Internal Server Error"}',
            'isBase64Encoded': False
        }
        self.assertDictEqual(dictionary, response)


if __name__ == '__main__':
    unittest.main()
