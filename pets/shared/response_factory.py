from json import dumps


class Response(object):

    def __init__(self, status_code: int):
        self.statusCode = status_code
        self.headers = {
            'Access-Control-Allow-Origin': "*"
        }

    def toJSON(self):
        return {
            "statusCode": self.statusCode,
            "headers": self.headers,
            "body": dumps(self.body),
            "isBase64Encoded": False
        }


class ResponseFactory(Response):
    def __init__(self, status_code, body=None):
        super(ResponseFactory, self).__init__(status_code)
        self.body = body

    @classmethod
    def ok_status(cls, body):
        return cls(200, body)

    @classmethod
    def error_client(cls, status_code, message):
        return cls(status_code, {"message": message})

    @classmethod
    def error_lambda(cls, message=None):
        return cls(503, {"message": message if message else 'Internal Server Error'})
