import boto3 as boto3
from boto3.dynamodb.conditions import Key

from pets import properties
from pets.infrastructure.db_services.db_service import DBService

AWS_REGION = properties.AWS_REGION
DB_TABLE_PETS = properties.DB_TABLE_PETS


class DynamoDB(DBService):

    def __init__(self):
        dynamo_db = boto3.resource('dynamodb', region_name=AWS_REGION)
        self.table_pets = dynamo_db.Table(DB_TABLE_PETS)

    def create_pet(self, pet):
        response = self.table_pets.put_item(
            Item=pet
        )
        return response

    def get_all_pets(self):
        response = self.table_pets.scan()
        return response['Items']

    def update_pet(self, pet):
        pet_id = pet['petId']
        string_expresion = "SET "
        values = {}
        names = {}
        is_first = True
        for field in pet:
            if field != "petId":
                if is_first:
                    is_first = False
                    string_expresion += "#" + field + "Att = :" + field + "Value"
                else:
                    string_expresion += ", #" + field + "Att = :" + field + "Value"
                values[':' + field + "Value"] = pet[field]
                names['#' + field + "Att"] = field
        self.table_pets.update_item(
            Key={'petId': pet_id},
            UpdateExpression=string_expresion,
            ExpressionAttributeValues=values,
            ExpressionAttributeNames=names,
            ReturnValues="UPDATED_NEW"
        )
        return True

    def get_one_pet(self, pet_id):
        response = self.table_pets.query(
            KeyConditionExpression=Key('petId').eq(pet_id)
        )
        return response['Items']

    def delete_pet(self, pet_id):
        response = self.table_pets.delete_item(
            Key={'petId': pet_id}
        )
        return response
