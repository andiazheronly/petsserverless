class DBService:

    def create_pet(self, pet):
        raise NotImplemented

    def get_all_pets(self):
        raise NotImplemented

    def update_pet(self, pet):
        raise NotImplemented

    def get_one_pet(self, pet_id):
        raise NotImplemented

    def delete_pet(self, pet_id):
        raise NotImplemented
