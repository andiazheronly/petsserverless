import json

from pets.exceptions import BadMethodException, BadRequestException, ApiException
from pets.infrastructure import DynamoDB
from pets.shared import ResponseFactory
from pets.use_cases import CreatePet, GetAllPets, GetOnePet, UpdatePets, DeletePet


class PetsService:
    def __init__(self):
        self.db_service = DynamoDB()
        self.resources = {
            "GET": {
                "/pets": GetAllPets,
                "/pets/{petId}": GetOnePet
            },
            "POST": {
                "/pets": CreatePet
            },
            "PUT": {
                "/pets/{petId}": UpdatePets
            },
            "DELETE": {
                "/pets/{petId}": DeletePet
            }
        }

    def process(self, event):
        try:
            http_method = event['httpMethod']
            resource = event['resource']
            parameters = None
            body = None

            if 'pathParameters' in event:
                parameters = event['pathParameters']
            if 'body' in event:
                body = json.loads(event['body'])

            response = self.__process(http_method, resource, parameters, body)
            return self.dispatch_response(
                ResponseFactory.ok_status(response)
            )
        except ApiException as e:
            return self.dispatch_response(
                ResponseFactory.error_client(e.status, e.message)
            )
        except BaseException as e:
            return self.dispatch_response(
                ResponseFactory.error_lambda()
            )

    def __process(self, http_method, resource, parameters, body):
        use_case = self.__validate_resource(http_method, resource)
        return use_case(self.db_service).execute(parameters=parameters, body=body)

    def __validate_resource(self, http_method, resource):
        resources_by_method = self.resources.get(http_method)
        if not resources_by_method:
            raise BadMethodException('METHOD_NOT_ALLOWED', 405)
        if resource in resources_by_method:
            return resources_by_method[resource]
        else:
            raise BadRequestException('BAD_REQUEST', 400)

    @staticmethod
    def dispatch_response(response):
        return response.toJSON()
