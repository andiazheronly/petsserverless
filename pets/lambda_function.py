from pets.rest import PetsService


def lambda_handler(event, context):
    return PetsService().process(event)
