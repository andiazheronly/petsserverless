from pets.exceptions import BadParameterException
from pets.exceptions.exceptions import ClientException
from pets.use_cases import UseCase


class GetOnePet(UseCase):

    def process_request(self, **kwargs):
        parameters = kwargs.get('parameters')
        if 'petId' not in parameters or not parameters['petId']:
            raise BadParameterException('BAD_REQUEST', 400)
        pet_id = parameters['petId']
        pet = self.db.get_one_pet(pet_id)
        if not pet:
            raise ClientException('NOT_FOUND', 404)
        return {"pet": pet[0]}
