import uuid

from pets.use_cases import UseCase


class CreatePet(UseCase):

    def process_request(self, **kwargs):
        body = kwargs.get("body")
        pet = self.valid_body(body)
        response = self.db.create_pet(pet)
        if response:
            return {"pet": pet}

    @staticmethod
    def valid_body(body):
        pet = {
            "petId": str(uuid.uuid4())
        }
        if 'name' in body and body['name']:
            pet['name'] = body['name']
        if 'owner' in body and body['owner']:
            pet['owner'] = body['owner']
        if 'type' in body and body['type']:
            pet['type'] = body['type']
        if 'size' in body and body['size']:
            pet['size'] = body['size']
        if 'description' in body and body['description']:
            pet['description'] = body['description']
        return pet
