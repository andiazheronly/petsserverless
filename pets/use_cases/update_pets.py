from pets.exceptions import BadParameterException
from pets.exceptions.exceptions import ClientException
from pets.use_cases import UseCase


class UpdatePets(UseCase):

    def process_request(self, **kwargs):
        parameters = kwargs.get('parameters')
        body = kwargs.get("body")
        if 'petId' not in parameters or not parameters['petId']:
            raise BadParameterException('BAD_REQUEST', 400)
        pet_id = parameters['petId']
        pets = self.db.get_one_pet(pet_id)
        if not pets:
            raise ClientException('NOT_FOUND', 404)
        pets = pets[0]
        pet = self.valid_body(pet_id, body)
        response = self.db.update_pet(pet)
        for field in pet:
            pets[field] = pet[field]
        if response:
            return {"pet": pets}

    @staticmethod
    def valid_body(pet_id, body):
        pet = {
            "petId": pet_id
        }
        if 'name' in body and body['name']:
            pet['name'] = body['name']
        if 'owner' in body and body['owner']:
            pet['owner'] = body['owner']
        if 'type' in body and body['type']:
            pet['type'] = body['type']
        if 'size' in body and body['size']:
            pet['size'] = body['size']
        if 'description' in body and body['description']:
            pet['description'] = body['description']
        return pet
