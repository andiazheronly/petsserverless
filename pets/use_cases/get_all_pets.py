from pets.exceptions.exceptions import ClientException
from pets.use_cases import UseCase


class GetAllPets(UseCase):

    def process_request(self, **kwargs):
        pets = self.db.get_all_pets()
        if not pets:
            raise ClientException('NOT_FOUND_PETS', 404)
        return {"pets": pets}
